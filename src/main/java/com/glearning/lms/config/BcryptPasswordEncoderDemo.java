package com.glearning.lms.config;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BcryptPasswordEncoderDemo {
	
	public static void main(String[] args) {
		String plainText = "welcome";
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		
		String encodedPassword1 = passwordEncoder.encode(plainText);
		String encodedPassword2 = passwordEncoder.encode(plainText);
		String encodedPassword3 = passwordEncoder.encode(plainText);
		String encodedPassword4 = passwordEncoder.encode(plainText);
		String encodedPassword5 = passwordEncoder.encode(plainText);
		
		
		System.out.println(encodedPassword1);
		System.out.println(encodedPassword2);
		System.out.println(encodedPassword3);
		System.out.println(encodedPassword4);
		System.out.println(encodedPassword5);
		
		
		System.out.println(passwordEncoder.matches("welcome", encodedPassword1));
		System.out.println(passwordEncoder.matches("welcome", encodedPassword2));
		System.out.println(passwordEncoder.matches("welcome", encodedPassword3));
		System.out.println(passwordEncoder.matches("welcome", encodedPassword4));
		System.out.println(passwordEncoder.matches("welcome", encodedPassword5));
	}

}
