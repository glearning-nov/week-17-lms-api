package com.glearning.lms.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppHealthConfig implements HealthIndicator{

	@Override
	public Health health() {
		return Health.down().withDetail("DB-status"," DB isrunning").build();
	}

}
