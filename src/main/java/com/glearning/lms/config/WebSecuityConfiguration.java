package com.glearning.lms.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.glearning.lms.service.DomainUserDetailsService;


@Configuration
//this is the bridge to spring security and our application
public class WebSecuityConfiguration extends WebSecurityConfigurerAdapter {
	
	//there are two points
	/*
	 * 1. Authentication
	 * 2. Authorization
	 */

	@Autowired
	private DomainUserDetailsService userDetailsService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	//This is to override the authorization
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.cors().disable();
		http.csrf().disable();
		http.headers().frameOptions().disable();
		
		http.authorizeRequests()
		.antMatchers("/h2-console/**", "/h2-console**")
			.permitAll()
		.antMatchers(HttpMethod.GET, "/api/library**")
			.hasAnyRole("USER", "ADMIN", "SUPER_ADMIN")
		.antMatchers(HttpMethod.POST, "/api/library**")
			.hasAnyRole("ADMIN", "SUPER_ADMIN")
		.antMatchers(HttpMethod.DELETE, "/api/library/**")
			.hasRole("SUPER_ADMIN")
		.anyRequest()
			.fullyAuthenticated()
		.and()
		.formLogin()
		.and()
		.httpBasic();
	}
	
	//For authentication
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(this.userDetailsService)
			.passwordEncoder(this.passwordEncoder);
	}
}
