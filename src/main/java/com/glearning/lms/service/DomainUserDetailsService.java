package com.glearning.lms.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.glearning.lms.model.User;
import com.glearning.lms.repository.UserRepository;

@Service
public class DomainUserDetailsService implements UserDetailsService{

	@Autowired
	private UserRepository userRepository;
	//this method accetps the username and fethces the user object from our UserJPARepository
	// Once the User is fetched, this method, returns the object of type UserDetails
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<User> optionalUser = this.userRepository.findByName(username);
		if(optionalUser.isPresent()) {
			User user = optionalUser.get();
			return new DomainUserDetails(user);
		}
		throw new UsernameNotFoundException("invalid username passed");
	}

}
