package com.glearning.lms.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.glearning.lms.model.Library;
import com.glearning.lms.repository.LibraryJpaRepository;

@Service
public class LibraryServiceImpl implements LibraryService {
	
	@Autowired
	private LibraryJpaRepository libraryRepository;

	@Override
	public String addSingleLibrary(Library library) {
		this.libraryRepository.save(library);
		return "Library saved :: ";
	}

	@Override
	public String addAllLibraries(List<Library> libraries) {
		this.libraryRepository.saveAll(libraries);
		return "all the libraries are saved in batch";
	}

	@Override
	public Library addLibraryWithSaveAndFlush(Library library) {
		return this.libraryRepository.saveAndFlush(library);
	}

	@Override
	public Map<String, Object> fetchAllLibraries(int page, int size, String strDirection, String property) {
		
		Sort.Direction direction = strDirection.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
		
		PageRequest pageRequest = PageRequest.of(page, size, direction, property);
		
		Page<Library> response = this.libraryRepository.findAll(pageRequest);
		
		
		int pages = response.getTotalPages();
		long count = response.getTotalElements();
		List<Library> content = response.getContent();
		
		Map<String, Object> responseMap = new LinkedHashMap<>();
		
		responseMap.put("pages", pages);
		responseMap.put("count", count);
		responseMap.put("content", content);
		
		
		return responseMap;
	}

	@Override
	public Library getLibraryById(long libraryId) {
		Optional<Library> optionalLibrary = this.libraryRepository.findById(libraryId);
		
		return optionalLibrary.orElseThrow();
	}

	@Override
	public void deleteLibraryById(long id) {
		this.libraryRepository.deleteById(id);
		
	}

	@Override
	public Library updateLibrary(long id, Library updatedLibrary) {
		/*
		 * first try to fetch the library with the existing id
		 * if libray exists, then update the library and save it
		 * else, throw an exception that the library with the given id does not exists
		 */
		Library existingLibrary = this.libraryRepository.findById(id).orElseThrow();
		existingLibrary.setName(updatedLibrary.getName());
		existingLibrary.setBooks(updatedLibrary.getBooks());

		return this.libraryRepository.save(existingLibrary);
	}

}
