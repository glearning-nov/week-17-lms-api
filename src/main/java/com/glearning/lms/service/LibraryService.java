package com.glearning.lms.service;

import java.util.List;
import java.util.Map;

import com.glearning.lms.model.Library;

public interface LibraryService {
	
	String addSingleLibrary(Library library);
	
	Library updateLibrary(long id, Library updatedLibrary);
	
	String addAllLibraries(List<Library> libraries);
	
	Library addLibraryWithSaveAndFlush(Library library);
	
	Map<String, Object> fetchAllLibraries(int page, int size, String direction, String property);
	
	Library getLibraryById(long libraryId);
	
	void deleteLibraryById(long id);

}
