package com.glearning.lms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibraryMenagementSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibraryMenagementSystemApplication.class, args);
	}

}
