package com.glearning.lms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.glearning.lms.model.Todo;

@RestController
@RequestMapping("/api/todos")
public class TodoRestController {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@GetMapping
	public List fetchTodos() {
		List response = this.restTemplate.getForObject("https://jsonplaceholder.typicode.com/todos", List.class);
		return response;
	}

}

